/// <reference types='cypress' />

describe('TICKETS', () => {
  beforeEach(() => cy.visit('https://ticket-box.s3.eu-central-1.amazonaws.com/index.html'))

  it('Fills all the text input fields', () => {
    const firstName = 'Petherson'
    const lastName = 'Erasmo'

    cy.get('#first-name').type(firstName)
    cy.get('#last-name').type(lastName)
    cy.get('#email').type('pet@test.com')
    cy.get('#requests').type('carnivorous')
    cy.get('#signature').type(`${firstName} ${lastName}`)
  })

  it('Select two tickets', () => {
    cy.get('#ticket-quantity').select('2')
  })

  it("Select 'Vip' ticket type", () => {
    cy.get('#vip').check()
  })

  it("Selects 'Social media' checkbox", () => {
    cy.get('#social-media').check()
  })

  it("Selects 'Friend' and 'publication', then uncheck 'Friend'", () => {
    cy.get('#friend').check()
    cy.get('#publication').check()
    cy.get('#friend').uncheck()
  })

  it('Alerts on invalid email', () => {
    cy.get('#email')
      .as('email') // exemplo de alias
      .type('Pet-test.com')

    cy.get('#email.invalid').should('exist')

    cy.get('@email')
      .clear()
      .type('pet@test.com')

    cy.get('#email.invalid').should('not.exist')
  })

  it("Has 'TICKETBOX' hearder's heding", () => {
    cy.get('header h1').should('contain', 'TICKETBOX')
  })

  it('Fills and reset the form', () => {
    const firstName = 'Petherson'
    const lastName = 'Erasmo'
    const fullName = `${firstName} ${lastName}`

    cy.get('#first-name').type(firstName)
    cy.get('#last-name').type(lastName)
    cy.get('#email').type('pet@test.com')

    cy.get('#ticket-quantity').select('2')

    cy.get('#vip').check()

    cy.get('#social-media').check()

    cy.get('#requests').type('carnivorous')

    cy.get('.agreement p').should(
      'contain',
            `I, ${fullName}, wish to buy 2 VIP tickets.`)

    cy.get('#agree').click()

    cy.get('#signature').type(`${fullName}`)

    cy.get('button[type="submit"]')
      .as('submitButton')
      .should('not.be.disabled')

    cy.get('button[type="reset"]').click()

    cy.get('@submitButton').should('be.disabled')
  })

  it('Fills mandatory fields using support command', () => {
    const customer = {
      firstName: 'João',
      lastName: 'Silva',
      email: 'joaosilva@example.com'
    }

    cy.fillMandatoryFields(customer)

    cy.get('button[type="submit"]')
      .as('submitButton')
      .should('not.be.disabled')

    cy.get('#agree').uncheck()

    cy.get('@submitButton').should('be.disabled')
  })
})
